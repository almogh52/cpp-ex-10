#include "Item.h"

/*
* This is the constructor of the class Item
* param name: The name of the item
* param serialNumber: The serial number of the item
* param unitPrice: The price of 1 unit of the item
*/
Item::Item(string name, string serialNumber, double unitPrice) : _name(name), _serialNumber(serialNumber), _unitPrice(unitPrice), _count(1)
{
}

/*
* This is the destructor of the Item class
*/
Item::~Item()
{
}

/*
* This function calculates the total price for this item
* return: The total price for this specific item
*/
double Item::totalPrice() const
{
	// The total price is the amount of units multiply by the unit price
	return _count * _unitPrice;
}

/*
* This function implements the operator < for the Item class
* param other: The other Item to compare with
* return: True if the other item has a bigger serial number than this Item
*/
bool Item::operator<(const Item & other) const
{
	// Convert the 2 serial numbers to numbers and compare them
	return std::stoi(_serialNumber) < std::stoi(other._serialNumber);
}

/*
* This function implements the operator > for the Item class
* param other: The other Item to compare with
* return: True if the other item has a smaller serial number than this Item
*/
bool Item::operator>(const Item & other) const
{
	// Convert the 2 serial numbers to numbers and compare them
	return std::stoi(_serialNumber) > std::stoi(other._serialNumber);
}

/*
* This function implements the operator == for the Item class
* param other: The other Item to compare with
* return: True if the other item has an identical serial number to this Item's serial number
*/
bool Item::operator==(const Item & other) const
{
	// Convert the 2 serial numbers to numbers and compare them
	return std::stoi(_serialNumber) == std::stoi(other._serialNumber);
}

// Getters
string Item::getName() const
{
	return _name;
}

string Item::getSerialNumber() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

// Setters
void Item::setName(string name)
{
	_name = name;
}

void Item::setSerialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setCount(int count)
{
	_count = count;
}

void Item::setUnitPrice(int unitPrice)
{
	_unitPrice = unitPrice;
}
