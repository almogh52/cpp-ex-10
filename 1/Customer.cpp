#include "Customer.h"

/*
* This is the constructor of the Customer class
* param name: The name of the customer
*/
Customer::Customer(string name) : _name(name)
{
}

/*
* This is the destructor of the Customer class
*/
Customer::Customer()
{
	// Clear the set of Items
	_items.clear();
}

/*
* This functions calculates the total sum of the items the customer has bought
* return: The total sum of the customer's buy
*/
double Customer::totalSum() const
{
	double sum = 0;

	// Go through the items the customer has added to his bucket
	for (const Item item : _items)
	{
		// Add each item's price to the total sum of the customer
		sum += item.totalPrice();
	}

	return sum;
}

/*
* This function adds an item to the set of items.
* param newItem: The new item to add to the customer's items set
* return: None
*/
void Customer::addItem(Item newItem)
{
	set<Item>::iterator itemIterator;

	// Try and find if the new item is already in the list
	itemIterator = _items.find(newItem);

	// If the iterator doesn't point to the end of the set, it means the item was found on the set
	if (itemIterator != _items.end())
	{
		// Increase the count amount of the item
		((Item *)&*itemIterator)->setCount(itemIterator->getCount() + 1);
	}
	else {
		// If the item isn't in the set yet, add it
		_items.insert(newItem);
	}
}

/*
* This function removes an item from the items set if it's found
* param item: The item to be removed
* return: None
*/
void Customer::removeItem(Item item)
{
	set<Item>::iterator itemIterator;

	// Try and find the item in the set
	itemIterator = _items.find(item);

	// If the iterator doesn't point to the end of the set, it means the item was found on the set, so erase it
	if (itemIterator != _items.end())
	{
		// Decrease the count of the item
		((Item *)&*itemIterator)->setCount(itemIterator->getCount() - 1);

		// If the count of the item reached 0, remove it from the items set
		if (itemIterator->getCount() == 0)
		{
			// Remove the item from the set
			_items.erase(itemIterator);
		}
	}
}

// Getters
string Customer::getName() const
{
	return _name;
}

set<Item>& Customer::getItems()
{
	return _items;
}

// Setters
void Customer::setName(string name)
{
	_name = name;
}

void Customer::setItems(set<Item> items)
{
	_items = items;
}
