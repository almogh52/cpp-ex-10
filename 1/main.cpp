#include"Customer.h"
#include<map>

#define NEW_CUSTOMER_OPTION 1
#define UPDATE_CUSTOMER_OPTION 2
#define MAX_CUSTOMER_OPTION 3
#define EXIT_OPTION 4

#define UPDATE_ADD_OPTION 1
#define UPDATE_REMOVE_OPTION 2
#define UPDATE_EXIT_OPTION 3

#define ITEM_LIST_AMOUNT 10

/*
* This functions checks if a given customer exists in the customers map
* param customerName: The name of the customer to be found
* param customers: The map of the customers
* return: True if the customer was found and false otherwise
*/
bool customerExists(string customerName, map<string, Customer> &customers)
{
	map<string, Customer>::iterator customerIterator;

	// Try to find the customer in the map of customers
	customerIterator = customers.find(customerName);

	// Check if the customer iterator is the end of the map, if it is, it means the customer wasn't found, else, it was found in the map
	return customerIterator != customers.end();
}

/*
* This function prints all the items in the item list
* param itemList: The list of items
* return: None
*/
void printItems(const Item itemList[])
{
	// Print explaination for the user
	std::cout << "The items you can buy are: (Enter 0 to exit)" << std::endl;

	// Print items
	for (int i = 0; i < ITEM_LIST_AMOUNT; i++)
	{
		// Print the number of the item + it's name and unit price
		std::cout << i + 1 << ". " << itemList[i].getName() << " - Price: " << itemList[i].getUnitPrice() << std::endl;
	}
}

/*
* This function gets items for a customer
* param customer: The customer to get items for
* return: None
*/
void getItemsForCustomer(Customer &customer, Item itemList[])
{
	int itemChoice = 0;

	// Clear the screen
	system("cls");

	// Print the items for the user
	printItems(itemList);
	std::cout << std::endl;

	// While the user didn't chose to finish, keep getting items
	do {
		// Get a choice from the user
		std::cout << "What item would you like to buy? Input: ";
		std::cin >> itemChoice;

		// Check if the item choice is a valid choice
		if (itemChoice > 0 && itemChoice <= ITEM_LIST_AMOUNT)
		{
			// Add the item to the customer's item list
			customer.addItem(itemList[itemChoice - 1]);
		}
	} while (itemChoice != 0);
}

/*
* This function gets a new customer from the user and adds it to the map of customers
* param customers: The map of customers
* param itemList: The list of items in the shop
* return: None
*/
void getNewCustomer(map<string, Customer> &customers, Item itemList[])
{
	string customerName;
	Customer newCustomer;

	// Get the name of the customer
	std::cout << "Please enter the name of the new customer: ";
	std::cin >> customerName;

	// If the customer already exists in the mpa of customers, send error and return
	if (customerExists(customerName, customers))
	{
		// Send error to the user and wait
		std::cout << "Customer already exist!\n";
		system("pause");

		return;
	}

	// Init the new customer
	newCustomer = Customer(customerName);

	// Get new items for the customer
	getItemsForCustomer(newCustomer, itemList);

	// Set the new customer in the map of customers
	customers[customerName] = newCustomer;
}

/*
* This function removes items for a customer
* param customer: The customer to remove for him items
* return: None
*/
void removeItemsForCustomer(Customer &customer)
{
	int i, choice = 0;
	set<Item>::iterator itemIterator;

	// While the user didn't chose to exit
	do {
		// Clear the screen
		system("cls");

		// Reset the counter of the items
		i = 1;

		// Print the customer's items
		std::cout << "Your items are:\n";
		for (Item item : customer.getItems())
		{
			// Print the item's name, count and price
			std::cout << i++ << ". " << item.getName() << " - Price: " << item.getUnitPrice() << " - Count: " << item.getCount() << std::endl;
		}
		std::cout << std::endl; // New line

		// Get the user's choice
		std::cout << "What item would like to remove? Input: ";
		std::cin >> choice;

		// Check if the user's choice is a valid item
		if (choice > 0 && choice <= customer.getItems().size())
		{
			// Set iterator to point at the start of the set
			itemIterator = customer.getItems().begin();

			// Advance the iterator to point at the object in the wanted index
			std::advance(itemIterator, choice - 1);

			// Remove the item that the iterator points to
			customer.removeItem(*itemIterator);
		}
	} while (choice != 0);
}

/*
* This function updates an existing customer
* param customers: The map of customers
* param itemList: The list of items in the shop
* return: None
*/
void updateExistingCustomer(map<string, Customer> &customers, Item itemList[])
{
	int choice = 0;
	string customerName;
	map<string, Customer>::iterator customerIterator;

	// Get the customer's name
	std::cout << "Please enter customer's name: ";
	std::cin >> customerName;

	// Try and find the customer, if not found, send error to user
	customerIterator = customers.find(customerName);
	if (customerIterator == customers.end())
	{
		// Send error to the user and wait
		std::cout << "Customer doesn't exist!\n";
		system("pause");

		return;
	}

	// While the user didn't enter to return to menu
	do {
		// Clear the screen
		system("cls");

		// Print menu
		std::cout << "Options:\n1. Add items\n2. Remove items\n3. Back to menu\n\n";

		// Get the user's choice
		std::cin >> choice;

		// Handle the user's choice
		switch (choice)
		{
		case UPDATE_ADD_OPTION:
			// Get new items for the customer
			getItemsForCustomer(customers[customerName], itemList);
			break;

		case UPDATE_REMOVE_OPTION:
			// Remove items for the customer
			removeItemsForCustomer(customers[customerName]);
			break;
		}
	} while (choice != UPDATE_EXIT_OPTION);
}

/*
* This function prints the customer with the max sum out of all the customers
* param customers: The map of the customers
* return: None
*/
void printMaxCustomer(map<string, Customer> &customers)
{
	Customer maxCustomer = Customer();

	// Go through the pairs in the map
	for (pair<string, Customer> p : customers)
	{
		// If the max customer is null (first check) or the customer has total sum bigger than the max customer, set it as the max customer
		if (p.second.totalSum() > maxCustomer.totalSum())
		{
			maxCustomer = p.second;
		}
	}

	// Print the max customers' name and sum
	std::cout << "\nThe customer with the max sum is " << maxCustomer.getName() << " with a sum of " << maxCustomer.totalSum() << "\n";

	// Wait for user click
	system("pause");
}

int main()
{
	int option = 0;
	map<string, Customer> abcCustomers;
	Item itemList[ITEM_LIST_AMOUNT] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	// While the selected option isn't the exit option
	do {
		// Clear the screen
		system("cls");

		// Print the menu for the user
		std::cout << "Welcome to MagshiMart!\nPlease choose an option:\n1. To sign as customer and buy items\n2. To update existing customer's items\n3. To print the customer who pays the most\n4. To exit\n\n";

		// Get the selected option from the user
		std::cin >> option;

		// Preform the wanted action for each 
		switch (option)
		{
		case NEW_CUSTOMER_OPTION:
			// Get a new customer from the user
			getNewCustomer(abcCustomers, itemList);
			break;

		case UPDATE_CUSTOMER_OPTION:
			// Update the wanted customer
			updateExistingCustomer(abcCustomers, itemList);
			break;

		case MAX_CUSTOMER_OPTION:
			// Print the customer with the max sum
			printMaxCustomer(abcCustomers);
			break;

		default:
			break;
		}
	} while (option != EXIT_OPTION);

	// Clear the map of customers
	abcCustomers.clear();

	return 0;
}